//
//  PaperCellTableView.swift
//  PaperCell
//
//  Created by Alexandr Gaidukov on 21/09/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import UIKit

public class DetailedTableView: UITableView {
    
    public var autoupdate: Bool = true
    
    public var cellOffsets = UIEdgeInsets(top: 7.0, left: 20.0, bottom: 7.0, right: 20.0)
    
    public var items: [DetailedCellConfigurationItem] = [] {
        didSet {
            if autoupdate {
                reloadData()
            }
        }
    }
    
    fileprivate var reuseIdentifiers: Set<String> = []
    fileprivate var detailedIndexPaths: Set<IndexPath> = []
    
    public override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        configure()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    private func configure() {
        separatorStyle = .none
        dataSource = self
        estimatedRowHeight = 45.0
        rowHeight = UITableView.automaticDimension
    }
    
    public func toggleCell(at indexPath: IndexPath, animation: DetailedCellAnimation? = nil, duration: TimeInterval = 0) {

        let isOpen = !detailedIndexPaths.contains(indexPath)
        
        if let cell = cellForRow(at: indexPath) as? DetailedCell {
            
            if isOpen {
                detailedIndexPaths.insert(indexPath)
            } else {
                detailedIndexPaths.remove(indexPath)
            }
            
            cell.toggle(animation: animation, duration: duration, isOpen: isOpen)
        }
        
        var totalDuration: TimeInterval = 0
        
        if animation != nil {
            totalDuration = duration
        }

        if isOpen {
            UIView.animate(withDuration: totalDuration) {
                self.beginUpdates()
                self.endUpdates()
                if self.detailedIndexPaths.contains(indexPath) {
                    self.scrollToRow(at: indexPath, at: .top, animated: true)
                }
            }
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + totalDuration) {
                self.beginUpdates()
                self.endUpdates()
            }
        }
        

    }
}

extension DetailedTableView: UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        let descriptor = item.cellDescriptor
        
        if !reuseIdentifiers.contains(descriptor.reuseIdentifier) {
            register(DetailedCell.self, forCellReuseIdentifier: descriptor.reuseIdentifier)
            reuseIdentifiers.insert(descriptor.reuseIdentifier)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: descriptor.reuseIdentifier, for: indexPath) as! DetailedCell
        
        if cell.headerView == nil && cell.detailView == nil {
            let headerView = Bundle.main.loadNibNamed(String(describing: descriptor.headerViewClass), owner: nil, options: nil)!.first as! UIView
            let detailView = Bundle.main.loadNibNamed(String(describing: descriptor.detailViewClass), owner: nil, options: nil)!.first as! UIView
            cell.offsets = cellOffsets
            cell.headerView = headerView
            cell.detailView = detailView
        }
        
        descriptor.configure(cell.headerView, cell.detailView)
        
        let isOpen = detailedIndexPaths.contains(indexPath)
        cell.toggle(isOpen: isOpen)
        
        return cell
    }
}
