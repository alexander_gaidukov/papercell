//
//  UIView+.swift
//  PaperCell
//
//  Created by Alexandr Gaidukov on 22/09/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import UIKit

extension UIView {
    func snapshot(fromRect rect: CGRect) -> UIView {
        let result = resizableSnapshotView(from: rect, afterScreenUpdates: true, withCapInsets: .zero)!
        result.backgroundColor = .clear
        return result
    }
}
