//
//  PaperCell.swift
//  PaperCell
//
//  Created by Alexandr Gaidukov on 21/09/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import UIKit

public enum DetailedCellAnimation {
    case folding(count: Int, backsideColor: UIColor)
    case flipAndSlide
}

extension DetailedCellAnimation {
    var animator: DetailedCellAnimator {
        switch  self {
        case .folding(let count, let color):
            return FoldAnimator(count: count, backsideColor: color)
        default:
            return FlipAndSlideAnimator()
        }
    }
}

class DetailedCell: UITableViewCell {
    
    private var detailViewBottomConstraint: NSLayoutConstraint!
    
    var animationView: UIView!
    
    var offsets = UIEdgeInsets(top: 7.0, left: 20.0, bottom: 7.0, right: 20.0)
    
    var animateViews: [UIView] = []
    
    var isAnimating: Bool = false
    
    var headerView: UIView! {
        didSet {
            
            contentView.addSubview(headerView)
            headerView.translatesAutoresizingMaskIntoConstraints = false
            
            contentView.topAnchor.constraint(equalTo: headerView.topAnchor, constant: -offsets.top).isActive = true
            contentView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: -offsets.left).isActive = true
            contentView.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: offsets.right).isActive = true
            
            let headerViewBottomConstraint = contentView.bottomAnchor.constraint(equalTo: headerView.bottomAnchor, constant: offsets.bottom)
            headerViewBottomConstraint.priority = UILayoutPriority(500)
            headerViewBottomConstraint.isActive = true
            
            organizeView()
        }
    }
    
    var detailView: UIView! {
        didSet {
            
            contentView.addSubview(detailView)
            detailView.translatesAutoresizingMaskIntoConstraints = false
            
            contentView.topAnchor.constraint(equalTo: detailView.topAnchor, constant: -offsets.top).isActive = true
            contentView.leadingAnchor.constraint(equalTo: detailView.leadingAnchor, constant: -offsets.left).isActive = true
            contentView.trailingAnchor.constraint(equalTo: detailView.trailingAnchor, constant: offsets.right).isActive = true
            
            detailViewBottomConstraint = contentView.bottomAnchor.constraint(equalTo: detailView.bottomAnchor, constant: offsets.bottom)
            detailViewBottomConstraint.priority = .defaultLow
            detailViewBottomConstraint.isActive = true
            
            organizeView()
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    private func configure() {
        selectionStyle = .none
        backgroundColor = .clear
    }
    
    private func organizeView() {
        if let detailView = detailView, let headerView = headerView {
            
            assert(detailView.frame.height > headerView.frame.height, "Detail View should be bigger than Header View")
            
            detailView.alpha = 0.0
            
            if animationView == nil {
                animationView = UIView(frame: .zero)
                animationView.backgroundColor = .clear
                animationView.layer.masksToBounds = true
                animationView.layer.cornerRadius = detailView.layer.cornerRadius
                contentView.addSubview(animationView)
                animationView.alpha = 0.0
                animationView.translatesAutoresizingMaskIntoConstraints = false
                detailView.topAnchor.constraint(equalTo: animationView.topAnchor).isActive = true
                detailView.leadingAnchor.constraint(equalTo: animationView.leadingAnchor).isActive = true
                detailView.trailingAnchor.constraint(equalTo: animationView.trailingAnchor).isActive = true
                detailView.bottomAnchor.constraint(equalTo: animationView.bottomAnchor).isActive = true
            }
            
            
            contentView.bringSubviewToFront(headerView)
        }
    }
    
    private func commonAnimationPerparation() {
        contentView.bringSubviewToFront(animationView)
        detailView.alpha = 1.0
        headerView.alpha = 1.0
    }
    
    private func commitAnimationPreparation(isOpen: Bool) {
        var transform = CATransform3DIdentity
        transform.m34 = -1.0 / 1000.0
        
        animationView.layer.sublayerTransform = transform
        
        animationView.alpha = 1.0
        headerView.alpha = 0.0
        detailView.alpha = 0.0
        detailViewBottomConstraint.priority = isOpen ? .defaultHigh : .defaultLow
    }
    
    func toggle(animation: DetailedCellAnimation? = nil, duration: TimeInterval = 0, isOpen: Bool) {
        
        guard !isAnimating else {
            return
        }
        
        if let animation = animation {
            let animator = animation.animator
            // Prepare for animation
            commonAnimationPerparation()
            animator.prepareForAnimation(cell: self, isOpen: isOpen)
            commitAnimationPreparation(isOpen: isOpen)
            
            //Perform animation
            animator.performAnimation(cell: self, duration: duration, isOpen: isOpen)
            
            return
        }
        
        headerView.alpha = isOpen ? 0.0 : 1.0
        detailView.alpha = isOpen ? 1.0 : 0.0
        contentView.bringSubviewToFront(isOpen ? detailView: headerView)
        detailViewBottomConstraint.priority = isOpen ? .defaultHigh : .defaultLow
    }
    
    func animationDidFinish(isOpen: Bool) {
        animateViews.removeAll()
        contentView.bringSubviewToFront(detailView)
        detailView.alpha = isOpen ? 1.0 : 0.0
        headerView.alpha = isOpen ? 0.0 : 1.0
        animationView.alpha = 0.0
        animationView.subviews.forEach{
            $0.removeFromSuperview()
        }
        isAnimating = false
    }
}
