//
//  FoldAnimator.swift
//  PaperCell
//
//  Created by Alexandr Gaidukov on 25/09/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import UIKit

final class FoldAnimator: NSObject, DetailedCellAnimator {
    
    private let count: Int
    private let backsideColor: UIColor
    
    init(count: Int, backsideColor: UIColor) {
        self.count = count
        self.backsideColor = backsideColor
    }
    
    func prepareForAnimation(cell: DetailedCell, isOpen: Bool) {
        let headerSnapshot = cell.headerView.snapshot(fromRect: cell.headerView.bounds)
        headerSnapshot.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0)
        headerSnapshot.frame.origin.y += headerSnapshot.frame.height / 2.0
        headerSnapshot.alpha = isOpen ? 1.0 : 0.0
        
        let firstPartSnapshot = cell.detailView.snapshot(fromRect: CGRect(origin: .zero, size: CGSize(width: cell.detailView.bounds.width, height: cell.headerView.bounds.height)))
        
        let secondPartSnapshot = cell.detailView.snapshot(fromRect: CGRect(origin: CGPoint(x: 0.0, y: cell.headerView.frame.height), size: CGSize(width: cell.detailView.bounds.width, height: min(cell.headerView.bounds.height, cell.detailView.frame.height - cell.headerView.frame.height))))
        secondPartSnapshot.layer.anchorPoint = CGPoint(x: 0.5, y: 0.0)
        secondPartSnapshot.frame.origin.y = cell.headerView.frame.height
        secondPartSnapshot.alpha = isOpen ? 0.0 : 1.0
        
        cell.animationView.addSubview(firstPartSnapshot)
        cell.animationView.addSubview(secondPartSnapshot)
        cell.animationView.addSubview(headerSnapshot)
        
        cell.animateViews.append(headerSnapshot)
        cell.animateViews.append(secondPartSnapshot)
        
        var currentHeight = firstPartSnapshot.frame.height + secondPartSnapshot.frame.height
        let diff = cell.detailView.frame.height - currentHeight
        
        if diff > 0 {
            let itemsCount = max(count - 1, 1)
            let step = diff / CGFloat(itemsCount * 2)
            
            for _ in 0..<itemsCount {
                let staticView = cell.detailView.snapshot(fromRect: CGRect(x: 0.0, y: currentHeight, width: cell.detailView.bounds.width, height: step))
                staticView.frame.origin.y = currentHeight
                staticView.alpha = isOpen ? 0.0 : 1.0
                
                let rotationView = cell.detailView.snapshot(fromRect: CGRect(x: 0.0, y: currentHeight + step, width: cell.detailView.bounds.width, height: step))
                rotationView.layer.anchorPoint = CGPoint(x: 0.5, y: 0.0)
                rotationView.frame.origin.y = currentHeight + step
                rotationView.alpha = isOpen ? 0.0 : 1.0
                
                let backView = BacksideView(frame: staticView.frame)
                backView.backgroundColor = backsideColor
                backView.layer.anchorPoint = CGPoint(x: 0.5, y: 1.0)
                backView.frame.origin.y += backView.frame.height / 2.0
                backView.alpha = 0.0
                backView.coveredView = staticView
                
                cell.animationView.addSubview(staticView)
                cell.animationView.addSubview(rotationView)
                cell.animationView.addSubview(backView)
                
                cell.animateViews.append(backView)
                cell.animateViews.append(rotationView)
                
                currentHeight += CGFloat(2.0) * step
            }
        }
    }
    
    func performAnimation(cell: DetailedCell, duration: TimeInterval, isOpen: Bool) {
        if !isOpen {
            cell.animateViews.reverse()
        }
        
        var fromValue: CGFloat = 0.0
        var toValue: CGFloat = isOpen ? -CGFloat.pi / 2.0 : CGFloat.pi / 2.0
        let animDuration = duration / TimeInterval(cell.animateViews.count)
        var timingFunction: CAMediaTimingFunctionName = .easeIn
        
        var delay: TimeInterval = 0
        
        for i in 0..<cell.animateViews.count {
            
            let animation = CABasicAnimation(keyPath: "transform.rotation.x")
            animation.timingFunction = CAMediaTimingFunction(name: timingFunction)
            animation.duration = animDuration
            animation.fromValue = fromValue
            animation.toValue = toValue
            animation.delegate = self
            animation.fillMode = .forwards
            animation.isRemovedOnCompletion = false
            animation.beginTime = CACurrentMediaTime() + delay
            
            
            let view = cell.animateViews[i]
            
            animation.setValue(view, forKey: "view")
            animation.setValue(isOpen ? i % 2 == 0 : true, forKey: "hide")
            animation.setValue(isOpen, forKey: "isOpen")
            animation.setValue(cell, forKey: "cell")
            
            view.layer.add(animation, forKey: "rotation.x")
            
            delay += animDuration
            fromValue = fromValue == 0.0 ? CGFloat.pi / 2.0 : CGFloat(0.0)
            toValue = toValue == 0.0 ? -CGFloat.pi / 2.0 : CGFloat(0.0)
            timingFunction = timingFunction == .easeIn ? .easeOut : .easeIn
            
            if !isOpen {
                fromValue = -fromValue
                toValue = -toValue
            }
        }
    }
}

extension FoldAnimator: CAAnimationDelegate {
    func animationDidStart(_ anim: CAAnimation) {
        guard let view = anim.value(forKey: "view") as? UIView,
            let isOpen = anim.value(forKey: "isOpen") as? Bool else {
                return
        }
        
        view.layer.shouldRasterize = true
        view.alpha = 1.0
        
        if let backView = view as? BacksideView, isOpen {
            backView.coveredView?.alpha = 1.0
        }
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        guard let view = anim.value(forKey: "view") as? UIView,
            let hide = anim.value(forKey: "hide") as? Bool,
            let isOpen = anim.value(forKey: "isOpen") as? Bool,
            let cell = anim.value(forKey: "cell") as? DetailedCell else {
                return
        }
        
        if let backView = view as? BacksideView, !isOpen {
            backView.coveredView.alpha = 0.0
        }
        
        view.layer.shouldRasterize = false
        view.alpha = hide ? 0.0 : 1.0
        view.layer.removeAllAnimations()
        
        if cell.animateViews.last == view {
            cell.animationDidFinish(isOpen: isOpen)
        }
    }
}

fileprivate final class BacksideView: UIView {
    var coveredView: UIView!
}
