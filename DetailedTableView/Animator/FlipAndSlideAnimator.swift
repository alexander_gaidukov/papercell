//
//  FlipAndSlideAnimator.swift
//  PaperCell
//
//  Created by Alexandr Gaidukov on 25/09/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import UIKit

final class FlipAndSlideAnimator: NSObject, DetailedCellAnimator {
    func prepareForAnimation(cell: DetailedCell, isOpen: Bool) {
        let headerViewSnapshot = cell.headerView.snapshot(fromRect: cell.headerView.bounds)
        let detailViewTopSnapshot = cell.detailView.snapshot(fromRect: CGRect(origin: .zero, size: CGSize(width: cell.detailView.frame.width, height: cell.headerView.frame.height)))
        let detailViewBottomSnapshot = cell.detailView.snapshot(fromRect: CGRect(x: 0.0, y: cell.headerView.frame.height, width: cell.detailView.frame.width, height: cell.detailView.frame.height - cell.headerView.frame.height))
        
        headerViewSnapshot.alpha = isOpen ? 1.0 : 0.0
        detailViewTopSnapshot.alpha = isOpen ? 0.0 : 1.0
        detailViewBottomSnapshot.alpha = isOpen ? 0.0 : 1.0
        detailViewBottomSnapshot.frame.origin.y = isOpen ? (cell.headerView.frame.height - detailViewBottomSnapshot.frame.height) : cell.headerView.frame.height
        
        cell.animationView.addSubview(detailViewBottomSnapshot)
        cell.animationView.addSubview(detailViewTopSnapshot)
        cell.animationView.addSubview(headerViewSnapshot)
        
        cell.animateViews = [headerViewSnapshot, detailViewTopSnapshot, detailViewBottomSnapshot]
    }
    
    private func slideAnimation(byValue: CGFloat, duration: TimeInterval, delay: TimeInterval) -> CABasicAnimation {
        
        let animation = CABasicAnimation(keyPath: "transform.translation.y")
        animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animation.duration = duration
        animation.byValue = byValue
        animation.delegate = self
        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = false
        animation.beginTime = CACurrentMediaTime() + delay
        return animation
    }
    
    private func flipAnimation(fromValue: CGFloat, toValue: CGFloat, timingFunction: CAMediaTimingFunctionName, duration: TimeInterval, delay: TimeInterval) -> CABasicAnimation {
        
        let animation = CABasicAnimation(keyPath: "transform.rotation.x")
        animation.timingFunction = CAMediaTimingFunction(name: timingFunction)
        animation.duration = duration
        animation.fromValue = fromValue
        animation.toValue = toValue
        animation.delegate = self
        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = false
        animation.beginTime = CACurrentMediaTime() + delay
        
        return animation
    }
    
    func performAnimation(cell: DetailedCell, duration: TimeInterval, isOpen: Bool) {
        
        if !isOpen {
            cell.animateViews.reverse()
        }
        
        var fromValue: CGFloat = 0.0
        var toValue: CGFloat = isOpen ? -CGFloat.pi / 2.0 : CGFloat.pi / 2.0
        let animDuration = duration / TimeInterval(4)
        var timingFunction: CAMediaTimingFunctionName = .easeIn
        var delay: TimeInterval = 0
        
        for i in 0...2 {
            
            let view = cell.animateViews[i]
            var animation: CABasicAnimation
            
            var isFlip: Bool = false
            
            if (isOpen && i == 2) || (!isOpen && i == 0) {
                animation = slideAnimation(byValue: isOpen ? view.frame.height : -view.frame.height, duration: duration / TimeInterval(2), delay: delay)
                delay += duration / TimeInterval(2)
            } else {
                animation = flipAnimation(fromValue: fromValue, toValue: toValue, timingFunction: timingFunction, duration: animDuration, delay: delay)
                delay += animDuration
                isFlip = true
            }
            
            animation.setValue(view, forKey: "view")
            animation.setValue(isOpen ? i == 0 : i != 2, forKey: "hide")
            animation.setValue(isOpen, forKey: "isOpen")
            animation.setValue(cell, forKey: "cell")
            
            view.layer.add(animation, forKey: "rotation")
            
            if isFlip {
                fromValue = fromValue == 0.0 ? CGFloat.pi / 2.0 : CGFloat(0.0)
                toValue = toValue == 0.0 ? -CGFloat.pi / 2.0 : CGFloat(0.0)
                timingFunction = timingFunction == .easeIn ? .easeOut : .easeIn
                
                if !isOpen {
                    fromValue = -fromValue
                    toValue = -toValue
                }
            }
        }
    }
}

extension FlipAndSlideAnimator: CAAnimationDelegate {
    func animationDidStart(_ anim: CAAnimation) {
        guard let view = anim.value(forKey: "view") as? UIView else {
                return
        }
        
        view.layer.shouldRasterize = true
        view.alpha = 1.0
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        guard let view = anim.value(forKey: "view") as? UIView,
            let hide = anim.value(forKey: "hide") as? Bool,
            let isOpen = anim.value(forKey: "isOpen") as? Bool,
            let cell = anim.value(forKey: "cell") as? DetailedCell else {
                return
        }
        
        view.layer.shouldRasterize = false
        view.alpha = hide ? 0.0 : 1.0
        view.layer.removeAllAnimations()
        
        if cell.animateViews.last == view {
            cell.animationDidFinish(isOpen: isOpen)
        }
    }
}
