//
//  DetailedCellAnimator.swift
//  PaperCell
//
//  Created by Alexandr Gaidukov on 25/09/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import Foundation

protocol DetailedCellAnimator {
    func prepareForAnimation(cell: DetailedCell, isOpen: Bool)
    func performAnimation(cell: DetailedCell, duration: TimeInterval, isOpen: Bool)
}
