//
//  CellDescriptor.swift
//  PaperCell
//
//  Created by Alexandr Gaidukov on 21/09/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import UIKit

public struct DetailedCellDescriptor {
    let reuseIdentifier: String
    let headerViewClass: UIView.Type
    let detailViewClass: UIView.Type
    let configure: (UIView, UIView) -> ()
    
    public init<Header: UIView, Detail: UIView>(reuseIdentifier: String, configure: @escaping (Header, Detail) -> ()) {
        self.reuseIdentifier = reuseIdentifier
        self.headerViewClass = Header.self
        self.detailViewClass = Detail.self
        
        self.configure = { header, detail in
            configure(header as! Header, detail as! Detail)
        }
    }
}

public protocol DetailedCellConfigurationItem {
    var cellDescriptor: DetailedCellDescriptor { get }
}
