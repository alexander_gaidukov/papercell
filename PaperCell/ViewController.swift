//
//  ViewController.swift
//  PaperCell
//
//  Created by Alexandr Gaidukov on 21/09/2017.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import UIKit
import DetailedTableView

struct Test: DetailedCellConfigurationItem {
    var cellDescriptor: DetailedCellDescriptor {
        return DetailedCellDescriptor(reuseIdentifier: "testCell", configure: { (header: HeaderView, detail: ContentView) in
            
        })
    }
}

class ViewController: UIViewController {
    
    @IBOutlet fileprivate weak var tableView: DetailedTableView!
    
    var items: [Test] = Array(repeating: Test(), count: 10)

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "background"))
        tableView.items = items
        tableView.delegate = self
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.toggleCell(at: indexPath, animation: .folding(count: 2, backsideColor: #colorLiteral(red: 0.954446784, green: 0.6905407488, blue: 0.7091529012, alpha: 1)), duration: 1.0)
        //self.tableView.toggleCell(at: indexPath, animation: .flipAndSlide, duration: 2.0)
    }
}

