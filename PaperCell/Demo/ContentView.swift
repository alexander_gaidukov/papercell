//
//  ContentView.swift
//  PaperCell
//
//  Created by Alexander Gaidukov on 9/22/17.
//  Copyright © 2017 Alexandr Gaidukov. All rights reserved.
//

import UIKit

class ContentView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.masksToBounds = true
        layer.cornerRadius = 4.0
    }
}
